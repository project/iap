<?php

/**
 * @file
 * Hooks provided by 'In-App Purchases Apple' for server-side developers.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add additional data to response array for Mobile client.
 *
 * You can uses it in ecommerce systems.
 * For example add item to response.
 *
 * @param obj $order_object
 *   Parsed response from App Store after verified.
 *   About keys see 'Verifying Store Receipts' http://tinyurl.com/cpof9qu.
 *
 * @param array $data
 *   Request data from Mobile client with 'receipt-data'.
 *
 * @return array
 *   Additional data to response for mobile.
 */
function hook_iap_apple_verify_after(&$order_object, &$data) {
  $item = array();

  // Get item by $order_object->product_id.
  $item[] = node_load($order_object->product_id);

  return $item;
}
