------------------------------------------------------------------------------
                            IN-APP PURCHASES MODULE
------------------------------------------------------------------------------
This module provides server-part for mobile in-app purchases.

Features:
    * Supporting Apple In-App Purchases
        * Verify In-App Purchase



MODULES LIST
------------------------------------------------------------------------------
* iap.module - module with configuration.
* iap_apple.module - module allows to verify receipt data on App Sore.



REQUIRES MODULES
------------------------------------------------------------------------------
1. Services

2. Rest_server



INSTALLING
------------------------------------------------------------------------------
1. Backup your database.

2. Make sure you resolved dependencies of the module.

3. Copy the complete 'iap/' directory into the 'sites/all/modules/',
   'sites/default/modules' or 'sites/name_of_your_site/modules' folder of
   your Drupal setup.
   More information about installing contributed modules could be found at
   "Install contributed modules" (http://drupal.org/node/70151).

4. Enable 'In-App Purchases' and 'In-App Purchases Apple' modules
   from the module administration page (Administration >> Modules).

5. Configure the module (see "CONFIGURATION" below).



CONFIGURATION
------------------------------------------------------------------------------
1. On the access control administration page ("Administration >> People
   >> Permissions") you need to assign:

   * "'Administer In-App Purchases Settings'" permission to the roles that are
     allowed to administer the 'In-App Purchases' settings.

2. Set type App Store server: Sandbox or Prod.
   Open 'In-App Purchases' config page 'admin/config/system/iap/apple'



FOR MOBILE DEVELOPERS
------------------------------------------------------------------------------
We use JSON data format and HTTP POST.

Verify process goes like this:
1. Mobile authenticates into Drupal and get SESSION
   http://your-drupal/iap/user/login

2. Mobile set SESSION in JSON array

3. Mobile set receipt data in JSON array

4. Mobile send JSON array to Drupal
   http://your-drupal/iap/iap_apple/verify

5. Mobile get response from Drupal with result of the verify


Example in Poster plugin for Firefox:
1. Log in
   * URL: http://your-drupal/iap/user/login
   * Content type: application/json
   * POST data as JSON array: {"username":"testuser","password":"testpassword"}

2. Get response after successful authorize
   Response example:
   <?XML VERSION="1.0" ENCODING="utf-8"?>
   <result>
   <sessid>bRnr4RHUf-0xrZx9Mt8b70UjF3RA0lPZv2KgZiFl5VM</sessid>
   <session_name>SESS70b7abf4a40cad39548a481878d17c0f</session_name>
   <user>
     <uid>12</uid>
     <name>testuser</name>
     <mail>testuser@your-drupal.com</mail>
     <theme></theme>
     <signature></signature>
     <signature_format>html_full</signature_format>
     <created>1352809435</created>
     <access>0</access>
     <login>1352809865</login>
     <status>1</status>
     <timezone>Europe/Moscow</timezone>
     <language>ru</language>
     <picture>0</picture>
     <init>testuser@your-drupal.com</init>
     <data><contact>1</contact></data>
     <roles is_array="true"><item>authenticated user</item></roles>
     <metatags/>
   </user>
   </result>

3. Define session: session_name = sessid
   Example JSON array for response above:
   {"SESS70b7abf4a40cad39548a481878d17c0f":"bRnr4RHUf-0xrZx9Mt8b70UjF3RA0lPZv2KgZiFl5VM"}

4. Add receipt data {"receipt-data":"value"} to JSON array.

5. Send receipt data to Drupal
   * http://your-drupal/iap/iap_apple/verify
   * Content type: application/json
   * POST data as JSON array:
     {"SESS70b7abf4a40cad39548a481878d17c0f":"bRnr4RHUf-0xrZx9Mt8b70UjF3RA0lPZv2KgZiFl5VM","receipt-data":"value"}

6. Successful response format:
   <?XML VERSION="1.0" ENCODING="utf-8"?>
   <result>
     <status>0</status>
     <data></data>
   </result>

   * Sucessful status has value equals 0.
   * Data contains addition info, e.g. item info, etc.
     Need to set server-side developer in hook_iap_apple_verify_after.



FOR SERVER-SIDE DEVELOPERS
------------------------------------------------------------------------------
1. All data stores in 'iap_apple_verify' table.

2. You can add additional data to to response array for Mobile client.
   Use hook_iap_apple_verify_after(&$order_object, &$data).



BUGS AND SHORTCOMINGS
------------------------------------------------------------------------------
* See the list of project issues [1].



AUTHORS AND MAINTAINERS
------------------------------------------------------------------------------
Maintainer: Alex Sorokin [2]



LINKS
------------------------------------------------------------------------------
[1] http://drupal.org/project/issues/1815858
[2] http://drupal.org/user/108088
